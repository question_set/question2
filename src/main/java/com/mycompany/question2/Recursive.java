package com.mycompany.question2;

/**
 * Recursive is a main class uses Functions class recursiveFunction method
 *
 * @author fatih
 */
public class Recursive {

    public static void main(String[] args) {
        Functions.recursiveFunction(9);        
    }
}
