package com.mycompany.question2;

/**
 * Functions class have a method recursiveFunction
 *
 * @author fatih
 */
public class Functions {

    /**
     * recursiveFunction method has a int parameter it recursively calling itself
     * at every step, parameter divided by 2 until it is small (or equal) from 2
     * if the parameter small(or equal) from 2 recursivation stops
     * @param i- an integer value
     *
     */
    public static void recursiveFunction(int i) {

        if (i <= 2) {
            System.out.println(i);
        } else {
            recursiveFunction(i / 2);
            System.out.println(i);

        }
    }

}
