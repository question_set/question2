package com.mycompany.question2;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class RecursiveTest
        extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public RecursiveTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(RecursiveTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        //the method to be tested is being created
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        
        Functions.recursiveFunction(9);
        
        //test content creation
        ByteArrayOutputStream testContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testContent));
        
        System.out.println("2");
        System.out.println("4");
        System.out.println("9");
        
        //testing
        assertEquals(testContent.toString(), outContent.toString());
    }
}
